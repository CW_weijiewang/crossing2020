(function($) {
  var allDESKTOP = window.matchMedia('(min-width: 769px)');
  var allMOBILE = window.matchMedia("(max-width: 970px)");

  // Header -----
  if (allMOBILE.matches) {
    $(document).ready(function(){
      $('.main-header').removeClass('main-header--big');
      $('.main-header').addClass('main-header--small');
      return false;
    });
  }

  $('.main-header--big .icon-burger').bind('click', function(event) {
    $('.main-header').addClass('main-header--show');
    return false;
  });

  $('.main-header .icon-close').bind('click', function(event) {
    $('.main-header').removeClass('main-header--show');
    return false;
  });

  // Searchbar
  $('#hSearch').bind('click', function(event) {
    $('.main-header').addClass('main-header--search');
    return false;
  });
  $('.main-header .icon-close').bind('click', function(event) {
    $('.main-header').removeClass('main-header--search');
    return false;
  });



  //Lazyload -----
  $('.lazy').lazy({
    effect: "fadeIn",
    effectTime: 2000,

    // loads instantly
    customLoaderName: function(element) {
      element.load();
    },

    // embed a youtube video
    youtubeLoader: function(element) {
      var url = 'https://www.youtube.com/embed/', frame = $('<iframe />')

      frame.attr('width', 700)
      frame.attr('height', 394)
      frame.attr('frameborder', 0)
      frame.attr('allow', 'accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture')
      frame.attr('src', url + element.data("video") + "?rel=0&autoplay=1" );

      element.append(frame).load();
    },

    // loads with a five seconds delay
    asyncLoader: function(element) {
      setTimeout(function() {
        element.load()
      }, 5000);
    },

    // always fail
    errorLoader: function(element) {
      element.error();
    }
  });



  // Do IE stuff -----
  // 超過行數文字變...
  if (window.document.documentMode) {
    var ellipsisText = function (e, etc) {
        var wordArray = e.innerHTML.split("");
        while (e.scrollHeight > e.offsetHeight) {
            wordArray.pop();
            e.innerHTML = wordArray.join("") + (etc || "...");
        }
    };
    [].forEach.call(document.querySelectorAll(".card .card__body .foreword"), function(elem) {
        ellipsisText(elem);
    });
  }


  // 導搜尋頁 -----
  $("#hSearchBTN").on('click',function() {
    window.location.href = "search.html";
  });
})(jQuery);